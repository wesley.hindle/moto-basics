import boto3
from moto import mock_s3
import os
import pytest

@pytest.fixture
@mock_s3
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def s3_client(aws_credentials):
    with mock_s3():
        yield boto3.client(
            service_name='s3',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
        )


def test_client_created(s3_client):
    # Test that the current AWS account can list all buckets in the account
    buckets = s3_client.list_buckets()["Buckets"]

    assert buckets is not None