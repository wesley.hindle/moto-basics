import boto3
from moto import mock_lambda
import os
import pytest

@pytest.fixture
@mock_lambda
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def aws(aws_credentials):
    with mock_lambda():
        yield (
            boto3.client(
            service_name='lambda',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
            ),
            boto3.client(
            service_name='cloudwatch',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
            ),
            boto3.client(
            service_name='logs', # Cloudwatch Logs?
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
            ),
            boto3.client(
            service_name='events',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
            ),
            boto3.client(
            service_name='iam',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
            ),
# [0] lambda, [1] cloudwatch, [2] logs, [3] eventbridge, [4] iam
        )

def test_lambda_exists(aws):
    func = aws[0].list_functions()
    assert func["Functions"][0]["FunctionName"] == "hello"