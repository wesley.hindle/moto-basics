# issues:
1. Docker image doesn't work
```bash
docker run motoserver/moto:latest
```

After running TF apply get this
```bash
Error: creating Amazon S3 (Simple Storage) Bucket (wesley-local-bucket): RequestError: send request failed
```
```bash
caused by: Put "http://localhost:5000/wesley-local-bucket": dial tcp [::1]:5000: connect: connection refused
```

Work around is to start the local server NOT as a docker img

# Testing Infrastructure with Moto
You may want to test your Terraform infrastructure without deploying it to an actual AWS account. You can do so using `moto server`. This creates a local AWS to which you can deploy to, using [custom endpoints in Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/guides/custom-service-endpoints).

[Server mode](http://docs.getmoto.org/en/latest/docs/server_mode.html) allows you to:
- Make API calls via the terminal
- Use a basic dashboard `http://localhost:5000` to see what you have deployed
- Write tests against the infrastructure deployed to `moto server`

## Setup

Pre Requisites:
- pip install 'moto[server]'
- Comfortable with using moto in testing
- Understanding of using Pytest
- Assumes you will use port 5000


1. Run the server (this defaults to port 5000):

```bash
docker run motoserver/moto:latest
```

or

```bash
moto_server
```

2. Setup basic Terraform config

```terraform
provider "aws" {
  region                        = "eu-west-2"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_requesting_account_id  = true
    # Optional if using S3
    s3_use_path_style           = true

  endpoints {
    # You can configure custom endpoints for multiple services
    #   On the same port
    <aws-service>       = "http://localhost:5000"
  }
}
```

Then run `terraform apply` to deploy the infra

3. Setup a basic Moto test:

```python
import boto3
from moto import mock_s3
import os
import pytest

@pytest.fixture
@mock_s3
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def <service>_client(aws_credentials):
    with mock_<service>():
        yield boto3.client(
            service_name='<service>',
            region_name='eu-west-2',
            endpoint_url='http://localhost:5000'
        )

```

4. You can communuicate with the `moto server` via API calls, the same as you would to communicate with AWS proper:

```bash
aws <service name> <functionality> <args> --endpoint-url=http://localhost:5000
```

---

## Deploying Terraform to Mock AWS
To deploy your Terraform infrastructure using `moto server`:

1. Start the server
```python
moto_server
```
---

2. Open a new terminal and deploy your Terraform. You can check this is being deployed to `moto` by seeing the API calls in the terminal `moto` is running in.
```bash
cd 2 && terraform apply
```
---

3. You can then verify your Terraform has been deployed in a number of ways:

3a. Check the Terraform state:
```bash
terraform state list
```

3b. Make an API call to AWS
```bash
aws lambda list-functions --endpoint-url=http://localhost:5000
```

3c. Check the moto dashboard:
Go to http://localhost:5000 and you should see some data on the services you have deployed.

---

## Writing Tests Against Moto Server
Once you have deployed infrastructure you may wish to write tests against this infrastructure. This can get very complex, very quickly and moto server may not be the most suitable tool for this.

A basic example of this:
```bash
cd mock_terraform/1 && moto_server
```

Open a new terminal and run:
```bash
terraform apply
```

```bash
pytest test_local.py
```


## Troubleshooting
- Installing `moto` is not the same as installing `moto[server]`. You should install this before further troubleshooting.
- When making API calls to `moto` if the errors mention `docker` then you likely are not using `moto[server]`
- Just because your Terraform has returned the `Apply complete!` you should double check no errors are present in the server terminal window.
- If `http://localhost:5000/moto-api` only shows a navigation bar then there is likely to be an error in the server terminal window. **awaiting response**
- `Error: failed to refresh cached credentials, operation error STS: AssumeRole, https response error StatusCode: 403, RequestID: 4cd69b29-3e5f-4014-9ebf-8fc4b9ee1194, api error AccessDenied: User: arn:aws:iam::825893415415:user/staff/wesley.hindle is not authorized to perform: sts:AssumeRole on resource: arn:aws:iam::825893415415:role/platform/DevOps` Check your `AWS_PROFILE` env var. This should be set to `default`.