import boto3
import pytz
from datetime import datetime, timedelta

# Password can only be 30 days old
# Convert current time date to be time zone aware needed for comparison with an AWS user date
current_time = pytz.timezone("UTC").localize(datetime.utcnow())
cutoff_date = current_time - timedelta(days=30)

def password_age_assessor(username):
    iam_client = boto3.client("iam")
    response = iam_client.get_login_profile(UserName=username)

    # Assigns the date the password was created
    password_age = response["LoginProfile"]["CreateDate"]

    #Just to show the creation time date
    print(password_age)

    #Just to show cutoff date
    print(cutoff_date)

    if password_age < cutoff_date:
        # This will delete the password
        iam_client.delete_login_profile(UserName=username)
        print("Not in date")
    else:
        print("In date")
