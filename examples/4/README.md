# Age-Based Testing
To test a condition that is date-based (such as password age, access key age etc) you need to create a mock AWS object you're wanting to test in the time period that will be used in your test. 

You can use the [freezegun library](https://github.com/spulec/freezegun) to do this.

*There is no explicit delete password option, you instead have to remove a user's login profile, preventing them from logging into the AWS console*

You can test this by running:
```bash
pytest examples/4/4_test.py
```