from socket import create_connection
import boto3
from app_4 import password_age_assessor
from moto import mock_iam
import os
import pytest
from freezegun import freeze_time

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        yield boto3.client("iam")

@freeze_time("01-01-2000")
def test_password_age_assessor(iam):
    username = "test.user"
    iam.create_user(Path='/staff/', UserName=username)
    iam.create_login_profile(
        UserName = username,
        Password = "Password1",
        PasswordResetRequired = False)

    # Checking that the user and login profile have been created
    assert iam.get_login_profile(UserName=username) is not None

    # To show the login profile has been created in the past
    creation_date = iam.get_login_profile(UserName=username)["LoginProfile"]["CreateDate"]
    print(creation_date)

    password_age_assessor(username)

    #Login profile has been deleted, so you check for that by asserting
    #An exception is raised when you try to retrieve the login profile
    with pytest.raises(Exception):
        iam.get_login_profile(UserName=username)
