A basic test showing how you can assign multiple instances to the same `yield` generator keyword. 

After the standard config for setting up moto you then make API calls using the two different objects. This may be necessary because for the same AWS service (ie IAM) not all methods are available to both [`resource`](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#service-resource) and [`client`](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#client)

You have generated a tuple, so when you pass this in as a parameter to later functions you will need to specify which of the tuple you want to use.

```python
@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        #client will be [0], resource [1]
        yield (boto3.client("iam"), boto3.resource("iam"))
```

When it comes to accessing a method of the object you want you need to specify the index of the tuple:

You use `boto3.client` here as `create_user` is a method you can call with this class
```python
iam[0].create_user(UserName = "user1")
```

You use `boto3.resource` here as `create_account_password_policy` is a method you can only call with this class
```python
iam[1].create_account_password_policy(MaxPasswordAge=1000)
```


Run the test with
```bash
pytest -s 2_test.py
```

This will show the username of the user you have just created `user1` and the new password policy you have set `MaxPasswordAge=1000`