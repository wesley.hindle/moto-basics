import boto3
from moto import mock_iam
import os
import pytest

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        #client will be [0], resource [1]
        yield (boto3.client("iam"), boto3.resource("iam"))

def test_client_created(iam):
    # Method used by client,
    iam[0].create_user(UserName = "user1")

    # Show user has been created
    print(iam[0].list_users()["Users"][0]["UserName"])

    # Method used by resource
    iam[1].create_account_password_policy(MaxPasswordAge=1000)

    # Shows the password policy has been changed
    print(iam[0].list_users()["Users"][0]["UserName"])