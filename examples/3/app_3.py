import boto3

def delete_login_profile(username):
    iam = boto3.client("iam")
    iam.delete_user(UserName=username)
