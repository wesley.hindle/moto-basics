import boto3
from app_3 import delete_login_profile
from moto import mock_iam
import os
import pytest

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        yield boto3.client("iam")

def test_delete_login_profile(iam):
    username = "user1"

    iam.create_user(UserName = username)

    username_before = iam.list_users()["Users"][0]["UserName"]

    # Show user has been created
    print(f"The user's name in AWS is {username_before} \n")

    delete_login_profile(username)

    # Show there are no users after the delete action has been mocked out.
    print(iam.list_users()["Users"])

    # Writing an actual test to check there are no users
    assert (iam.list_users()["Users"]) == []

