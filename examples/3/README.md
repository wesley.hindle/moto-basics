The code in `app_3` will delete a specific user. To test this without deleting any actual users we can use moto. 

**Running this code will not delete an actual user and will return an error.**

`app_3.py` will take a username and delete the user with that name

`3_test.py` tests this by:
- Setting up basic moto config
- Creating a dummy user and checking it exists
- Calling the function we want to test (which deletes our dummy user)
- Then list all AWS users, which should return an empty list
- We then write an assertion saying that we expect this

Using moto for tests like this will require extra setup e.g. creating a test user.