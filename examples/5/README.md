# Manipulating the backend

You may want to amend resources based on when they were used. However, this action is not available through API calls (e.g. accessing the console). You need to manipulate an element to be date in the past, but `freezegun` can't always help with this.

If you want to check when a password was last used, you would need to use that password to log into the console and there's no way to do that via API calls. Freezegun would only be able to help with amending the date the password was *created* not used.

To change this you need a more complex test, requiring more setup. This means amending setting in moto's internal API, which can **change without notification** and possibly break your tests.

Within IAM there is a [`User` class](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#user) which takes an IAM user and allows you to see various attributes, such as when you last used your password (`password_last_used`).

For use with moto we need to change the `User`, to be a user we create, so we are able to manipulate the `User` attributes. This is the extra setup needed before we can actually test what we want to test.

---
## How to:

1. Start by importing these modules from boto
```python
from moto.backends import get_backend
from moto.core import DEFAULT_ACCOUNT_ID as ACCOUNT_ID
```

2. Setup your mocked `aws_credentials` and `iam` fixtures as before.

3. Set a time in the past you want to manipulate
```python
current_time = pytz.timezone("UTC").localize(datetime.utcnow())
password_last_used_date = current_time - timedelta(days=100)
```

4. Create a fake user with a `login_profile` and `access_key`.
```python
username = "test.user"
    iam[0].create_user(Path='/staff/', UserName=username)
    iam[0].create_login_profile(
        UserName = username,
        Password = "Password1",
        PasswordResetRequired = False
    )

    access_key = iam[0].create_access_key(UserName=username)["AccessKey"]
```

5. Then set maniuplate the IAM user's `passworrd_last_used` date
```python
iam_backend = get_backend("iam")[ACCOUNT_ID]["global"]
iam_backend.users[username].password_last_used = password_last_used_date
```

---

## Example
You can see this in action by running
```bash
pytest examples/5/5_test.py
```

The test creates a new user and sets their `password_last_used` date to be 100 days ago. This user is then passed as a parameter into `app_5.py` which checks if their password was last used over 30 days ago. If it was their `login_profile` is deleted.