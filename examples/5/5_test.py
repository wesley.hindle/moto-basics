from app_5 import password_last_used
import boto3
from datetime import datetime, timedelta
from moto import mock_iam
from moto.backends import get_backend
from moto.core import DEFAULT_ACCOUNT_ID as ACCOUNT_ID
import os
import pytest
import pytz

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        #client will be [0], resource [1]
        yield (boto3.client("iam"), boto3.resource("iam"))


def test_password_last_used(iam):
    current_time = pytz.timezone("UTC").localize(datetime.utcnow())
    password_last_used_date = current_time - timedelta(days=100)

    username = "test.user"
    iam[0].create_user(Path='/staff/', UserName=username)
    iam[0].create_login_profile(
        UserName = username,
        Password = "Password1",
        PasswordResetRequired = False
    )

    iam_backend = get_backend("iam")[ACCOUNT_ID]["global"]
    iam_backend.users[username].password_last_used = password_last_used_date

    assert iam[0].get_login_profile(UserName=username) is not None

    password_last_used(username)

    with pytest.raises(Exception):
        iam[0].get_login_profile(UserName=username)
