import boto3
import pytz
from datetime import datetime, timedelta

# Password can only be 30 days old
# Convert current time date to be time zone aware needed for comparison with an AWS user date
current_time = pytz.timezone("UTC").localize(datetime.utcnow())
cutoff_date = current_time - timedelta(days=30)

def password_last_used(username):
    iam_resource = boto3.resource("iam")
    iam_client = boto3.client("iam")
    aws_user = iam_resource.User(username)

    if aws_user.password_last_used < cutoff_date:
        print("not fine")
        iam_client.delete_login_profile(UserName=username)
