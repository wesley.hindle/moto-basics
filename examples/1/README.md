# Testing Basic Moto config

This is the basic config for using the moto library.

Run using
`pytest -s 1_test.py`
*Note: the `-s` flag is needed to show the output of print statements*

Which should return:
```bash
{'Users': [], 'IsTruncated': False, 'ResponseMetadata': {'RequestId': '7a62c49f-347e-4fc4-9331-6e8eEXAMPLE', 'HTTPStatusCode': 200, 'HTTPHeaders': {'server': 'amazon.com'}, 'RetryAttempts': 0}}
```

Showing you have made a successful mocked out AWS call as no users have been returned but have made a successful API call.