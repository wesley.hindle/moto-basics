import boto3
from moto import mock_iam
import os
import pytest

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture
def iam_client(aws_credentials):
    with mock_iam():
        yield boto3.client("iam")


def test_client_created(iam_client):
    users = iam_client.list_users()
    print(users)