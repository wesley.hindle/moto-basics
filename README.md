# Boto3 Testing - Moto
[Moto](http://docs.getmoto.org/en/latest/) is a mocking library used for replacing real calls to AWS with mock calls to the moto library for Python testsing. Allowing you to testing your app without the risk of changing your actual infrastructure.

Moto does not support [all AWS services](https://github.com/getmoto/moto/blob/master/IMPLEMENTATION_COVERAGE.md) and their methods.

Moto is designed to be used alongside Python testing libraries (e.g. unittest or Pytest). If you are new to testing with Python look at this repo first **link to pytest basics**

## Getting Started

*This walkthrough will mainly focus on the IAM service*

### Pre requisites:
- Basic knowledge of Python testing
- Basic knowledge of Pytest library
- You can make API calls to AWS (test by running `aws sts get-caller-identity`)
- Your `AWS_PROFILE` env var is not set to a role **Clarification needed**
- Installed via pip:
    - boto3
    - pytest
    - moto >=4.1.3
    - freezegun

*Note: You should specify which `moto` services you wish to install using `pip install "moto[service]"`. All `moto` dependencies are not installed when using `pip install moto`.*


### Basic Config

Setup the backend config so you make calls to moto, not AWS.
```python
import boto3
from moto import mock_<service-name>
import os
import pytest
```

Fixtures run this code before any tests and setup conditions needed before tests can be run
```python
@pytest.fixture
```

This decorator is used to all AWS calls within it
```python
@mock_iam
```

Set dummy credentials so real AWS infrastructure is not affected by tests.
```python
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
```

Complete minimalist setup
```python
import boto3
from moto import mock_<service-name>
import os
import pytest

@pytest.fixture
@mock_iam
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
```

---

### Adding a moto service
Next you will need to create an instance of the boto class. Depending on what functionality you need will dictate whether you need a `client` or `resource` instance.
Both are instantiated the same way.

Pass in the fake credentials created above as a parameter. This will also pass in the `@mock_iam` functionality from the `aws_credentials` function, so any IAM calls made with *this* function will also be mocked out.
```python
@pytest.fixture
def iam_client(aws_credentials):
```

This creates an instance of the `boto3.client` for the IAM class. `Yield` is a [generator](https://docs.python.org/3/glossary.html#term-generator), so this instance is now available throughout all future functions which take this function as a parameter.
```python
    with mock_iam():
        yield boto3.client("iam")
```

You can test this works by running:
```bash
pytest examples/1/1_test.py
```
---

Instance of `boto3.client`
```python
@pytest.fixture
def iam_client(aws_credentials):
    with mock_iam():
        yield boto3.client("iam")
```

Instance of `boto3.resource`
```python
@pytest.fixture
def iam_resource(aws_credentials):
    with mock_iam():
        yield boto3.resource("iam")
```


If need both a `client` and `resource` instance you can yield a tuple in a fixture and create both at the same time:
You can test this works by running `examples/2/2_test.py`
```python
@pytest.fixture
def iam(aws_credentials):
    with mock_iam():
        yield (boto3.client("iam"), boto3.resource("iam"))
```

## Testing with Moto
Now that the basics are set you can get started actually using it for something useful - testing!


1. Deleting a user in IAM: See `examples/3/README.md`

2. Testing AWS resources where the code checks conditions based on date: See `examples/4/README.md`

3. Test AWS resources where conditions are based on checking when something was last used: See `examples/4/README.md`

4. Test conditions are based on when an element (e.g. password) was last used, but this element is not accessible via API calls: See `examples/5/README.md`

## Testing Infrastructure with Moto

You are also able to deploy AWS infrastructure to a mocked AWS account using `moto`. See the `mock_terraform/README.md` for details.


## Support
- Some FAQs are available in the [docs](http://docs.getmoto.org/en/latest/docs/faq.html)
- You can ask questions by raising one on their [GitHub page](https://github.com/getmoto/moto/issues). They are generally helpful and respond within a day.